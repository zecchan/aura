﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public class AuraConfig
    {
        public double? WindowWidth { get; set; }
        public double? WindowHeight { get; set; }
        public double? Left { get; set; }
        public double? Top { get; set; }
        public string Name { get; set; }
        public bool Topmost { get; set; }

        public string Avatar { get; set; } = "avatar";
    }
}
