﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura
{
    public class AvatarWindowStateArgument
    {
        /// <summary>
        /// Window location relative to the screen
        /// </summary>
        public Point WindowLocation { get; set; }
        public Size WindowSize { get; set; }
        /// <summary>
        /// Avatar location relative to the window
        /// </summary>
        public Point AvatarLocation { get; set; }
        /// <summary>
        /// Avatar location relative to the screen
        /// </summary>
        public Point AbsoluteAvatarLocation { get => new Point(WindowLocation.X + AvatarLocation.X, WindowLocation.Y + AvatarLocation.Y); }
        public Size AvatarSize { get; set; }
    }
}
