﻿using Aura.Graphic;
using AuraCore.Persona;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public static class AI
    {
        public static Avatar Avatar { get; private set; }

        public static AuraConfig Config { get; private set; } = new AuraConfig();

        public static AuraPersona Persona { get; private set; } = new AuraPersona();

        public static bool UIReady { get; set; } = false;

        public static List<IAuraEventHandlerPlugin> EventHandlerPlugins { get; } = new List<IAuraEventHandlerPlugin>();

        public static List<IAuraPlugin> AllPlugins { get; } = new List<IAuraPlugin>();

        public static void DispatchEvent(string eventId, object eventData)
        {
            var args = new EventDispatchedEventArgument();
            foreach (var pl in EventHandlerPlugins)
            {
                pl.HandleEvent(Avatar, Persona, eventId, eventData, args);
                if (args.Handled) return;
            }
            EventDispatched?.Invoke(eventId, eventData, args);
        }

        public static IEnumerable<T> RequestData<T>(string dataId)
        {
            var res = new List<T>();
            var invokeList = DataRequested?.GetInvocationList();
            foreach(var del in invokeList)
            {
                try
                {
                    var re = del.DynamicInvoke(dataId, typeof(T));
                    if (re is T)
                    {
                        res.Add((T)re);
                    }
                }
                catch { }
            }
            return res;
        }

        private static string LastLoadedAvatar { get; set; }
        public static void LoadAvatar(string path)
        {
            var json = File.ReadAllText(path);
            Avatar = JsonConvert.DeserializeObject<Avatar>(json);
            LoadedDLLs.Clear();
            LastLoadedAvatar = path;
        }

        public static void ReloadAvatar(string path = null)
        {
            if (Avatar == null) return;
            path = path ?? LastLoadedAvatar;

            var json = File.ReadAllText(path);
            var na = JsonConvert.DeserializeObject<Avatar>(json);
            if (na != null)
            {
                Avatar.Drawables = na.Drawables;
                Avatar.Conditions.Clear();
                foreach (var cond in na.Conditions)
                    Avatar.Conditions[cond.Key] = cond.Value;
                Avatar.Psyches.Clear();
                foreach (var cond in na.Psyches)
                    Avatar.Psyches[cond.Key] = cond.Value;
                Avatar.Regions.Clear();
                foreach (var cond in na.Regions)
                    Avatar.Regions[cond.Key] = cond.Value;
                LastLoadedAvatar = path;
            }
        }

        private static List<string> LoadedDLLs { get; } = new List<string>();
        public static void LoadPlugins(string path)
        {
            if (Avatar == null) throw new Exception("Avatar is not loaded!");
            if (File.Exists(path))
            {
                if (LoadedDLLs.Contains(path.ToLower())) return;
                LoadedDLLs.Add(path.ToLower());

                var asm = Assembly.LoadFile(Path.GetFullPath(path));
                var plugins = asm.ExportedTypes.Where(x => typeof(IAuraPlugin).IsAssignableFrom(x));

                var plg = new List<IAuraPlugin>();
                foreach (var plugin in plugins)
                {
                    var instance = Activator.CreateInstance(plugin) as IAuraPlugin;
                    if (instance == null) continue;
                    plg.Add(instance);
                    if (instance is IAuraStatePlugin aap)
                        Persona.State.Plugins.Add(aap);
                    if (instance is IAuraMessageProviderPlugin ampp)
                        Persona.MessagePlugins.Add(ampp);
                    if (instance is IAuraEventHandlerPlugin aehp)
                        EventHandlerPlugins.Add(aehp);
                    AllPlugins.Add(instance);
                }
                foreach (var p in plg) p.LoadState();
            }
            else if (Directory.Exists(path))
            {
                var fls = Directory.EnumerateFiles(path, "*.dll", SearchOption.AllDirectories);
                foreach (var f in fls)
                    LoadPlugins(f);
            }
        }
        public static void LoadConfig(string path)
        {
            if (File.Exists(path))
            {
                var json = File.ReadAllText(path);
                Config = Newtonsoft.Json.JsonConvert.DeserializeObject<AuraConfig>(json);
            }
        }

        public static void SaveConfig(string path)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(Config);
            File.WriteAllText(path, json);
        }

        public static void SaveMemory()
        {
            var aiPath = Path.GetFullPath("ai");
            if (!Directory.Exists(aiPath))
                Directory.CreateDirectory(aiPath);

            JsonConvert.DefaultSettings = () =>
            {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add(new MemoryValueConverter());
                settings.Converters.Add(new EmotionConverter());
                return settings;
            };

            // save tags 
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(Persona.State.Tags);
            File.WriteAllText(Path.Combine(aiPath, "tags.mem"), json);
            // save tag biases
            json = Newtonsoft.Json.JsonConvert.SerializeObject(Persona.State.TagBias);
            File.WriteAllText(Path.Combine(aiPath, "tagbias.mem"), json);
            // save active memory
            json = Newtonsoft.Json.JsonConvert.SerializeObject(Persona.Memory.ActivatedMemories);
            File.WriteAllText(Path.Combine(aiPath, "active.mem"), json);
            // save stored memory
            json = Newtonsoft.Json.JsonConvert.SerializeObject(Persona.Memory.StoredMemories);
            File.WriteAllText(Path.Combine(aiPath, "stored.mem"), json);
        }

        public static void LoadMemory()
        {
            var aiPath = Path.GetFullPath("ai");
            if (!Directory.Exists(aiPath))
                Directory.CreateDirectory(aiPath);

            JsonConvert.DefaultSettings = () =>
            {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add(new MemoryValueConverter());
                settings.Converters.Add(new EmotionConverter());
                return settings;
            };

            var path = Path.Combine(aiPath, "tags.mem");
            if (File.Exists(path))
            {
                var json = File.ReadAllText(path);
                Persona.State.Tags.Clear();
                Persona.State.Tags.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(json));
            }
            path = Path.Combine(aiPath, "tagbias.mem");
            if (File.Exists(path))
            {
                var json = File.ReadAllText(path);
                Persona.State.TagBias.Clear();
                var re = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, KeyValuePair<string, int>>>(json);
                foreach (var kv in re)
                    Persona.State.TagBias.Add(kv.Key, kv.Value);
            }
            path = Path.Combine(aiPath, "active.mem");
            if (File.Exists(path))
            {
                var json = File.ReadAllText(path);
                Persona.Memory.ActivatedMemories.Clear();
                Persona.Memory.ActivatedMemories.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<MemoryValue>>(json));
            }
            path = Path.Combine(aiPath, "stored.mem");
            if (File.Exists(path))
            {
                var json = File.ReadAllText(path);
                Persona.Memory.StoredMemories.Clear();
                var re = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, MemoryValue>>(json);
                foreach (var kv in re)
                    Persona.Memory.StoredMemories.Add(kv.Key, kv.Value);
            }
        }

        public static T LoadPluginData<T>(string id) where T : class, new()
        {
            var saveDir = "data\\plugindata\\";
            var fpath = Path.Combine(saveDir, id + ".json");
            if (!File.Exists(fpath)) return null;
            var json = File.ReadAllText(fpath);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }
        public static void SavePluginData<T>(string id, T data) where T : class, new()
        {
            var saveDir = "data\\plugindata\\";
            var fpath = Path.Combine(saveDir, id + ".json");
            var dir = Path.GetDirectoryName(fpath);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            File.WriteAllText(fpath, json);
        }

        public static event EventDispatchedEvent EventDispatched;

        public static event DataRequestEvent DataRequested;

        public static void Shutdown()
        {
            // save config and memory
            try
            {
                SaveConfig("config.json");
                SaveMemory();
                DispatchEvent("shutdown", null);
            }
            catch { }
            foreach(var p in AllPlugins)
            {
                try
                {
                    p.SaveState();
                    p.Shutdown();
                }
                catch { }
            }
        }
    }

    public delegate void EventDispatchedEvent(string eventId, object eventData, EventDispatchedEventArgument e);

    public delegate object DataRequestEvent(string dataId, Type preferedDataType);

    public class EventDispatchedEventArgument
    {
        /// <summary>
        /// When set to true, disable further call chaining.
        /// </summary>
        public bool Handled { get; set; }
    }
}
