﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuraCore.Persona
{
    /// <summary>
    /// Refer to: https://en.wikipedia.org/wiki/Emotion#/media/File:Geneva_Emotion_Wheel_-_English.png
    /// </summary>
    public struct Emotion
    {
        /// <summary>
        /// The pleasure value of the emotion. Positive being pleasant, negative being unpleasant. Value should, but not must, range from -100 to 100.
        /// </summary>
        public double Pleasure;

        /// <summary>
        /// The alertness of the emotion. Positive being activated, negative being calm. Value should, but not must, range from -100 to 100.
        /// </summary>
        public double Alertness;

        /// <summary>
        /// The strength of the emotion, this affect how likely this memory to be stored to long-term memory. The value should be around 0 to 141.421356
        /// </summary>
        public double Strength { get => Math.Sqrt(Math.Pow(Pleasure, 2) + Math.Pow(Alertness, 2)); }

        /// <summary>
        /// Gets the neutral emotion.
        /// </summary>
        public static readonly Emotion Neutral = new Emotion() { Alertness = 0, Pleasure = 0 };

        public Emotion Clamped
        {
            get => Clamp(-100, 100);
        }

        public Emotion Clamp(double min, double max)
        {
            return new Emotion()
            {
                Alertness = Math.Max(min, Math.Min(max, Alertness)),
                Pleasure = Math.Max(min, Math.Min(max, Pleasure)),
            };
        }

        /// <summary>
        /// Gets the distance between this emotion and another emotion.
        /// </summary>
        public double DistanceTo(Emotion emotion)
        {
            return Math.Sqrt(Math.Pow(emotion.Pleasure - Pleasure, 2) + Math.Pow(emotion.Alertness - Alertness, 2));
        }

        public static Emotion operator +(Emotion a, Emotion b)
        {
            return new Emotion()
            {
                Alertness = a.Alertness + b.Alertness,
                Pleasure = a.Pleasure + b.Pleasure
            };
        }

        public static Emotion operator -(Emotion a, Emotion b)
        {
            return new Emotion()
            {
                Alertness = a.Alertness - b.Alertness,
                Pleasure = a.Pleasure - b.Pleasure
            };
        }

        /// <summary>
        /// This is actually result to the average between two emotions
        /// </summary>
        public static Emotion operator ^(Emotion a, Emotion b)
        {
            return new Emotion()
            {
                Alertness = (a.Alertness + b.Alertness) / 2,
                Pleasure = (a.Pleasure + b.Pleasure) / 2
            };
        }

        public static Emotion operator *(Emotion a, double multiplier)
        {
            return new Emotion()
            {
                Alertness = a.Alertness * multiplier,
                Pleasure = a.Pleasure * multiplier
            };
        }

        public static Emotion operator /(Emotion a, double divider)
        {
            return new Emotion()
            {
                Alertness = a.Alertness / divider,
                Pleasure = a.Pleasure / divider
            };
        }
    }

    public static class EmotionValues
    {
        public const double LowPleasure = 10;
        public const double MediumPleasure = 25;
        public const double HighPleasure = 40;
        public const double VeryHighPleasure = 60;

        public const double LowDispleasure = -10;
        public const double MediumDispleasure = -25;
        public const double HighDispleasure = -40;
        public const double VeryHighDispleasure = -60;

        public const double LowAlert = 10;
        public const double MediumAlert = 25;
        public const double HighAlert = 40;
        public const double VeryHighAlert = 60;

        public const double LowCalm = -10;
        public const double MediumCalm = -25;
        public const double HighCalm = -40;
        public const double VeryHighCalm = -60;
    }
}
