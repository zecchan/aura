﻿using Aura;
using Aura.Graphic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuraCore.Persona
{
    public class AuraPersonaState
    {
        public List<string> Tags { get; } = new List<string>();

        public Dictionary<string, KeyValuePair<string, int>> TagBias { get; } = new Dictionary<string, KeyValuePair<string, int>>();

        Dictionary<string, int> conditionalTagBias { get; } = new Dictionary<string, int>();
        public Dictionary<string, int> ConditionalTagBias { get => conditionalTagBias; }

        public List<IAuraStatePlugin> Plugins { get; } = new List<IAuraStatePlugin>();

        public Dictionary<string, int> CurrentTagPower
        {
            get
            {
                var ret = Tags.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.Trim().ToLower()).Distinct().ToDictionary(x => x, y => Tags.Count(w => w?.ToLower().Trim() == y));
                foreach (var tb in TagBias)
                {
                    if (ret.ContainsKey(tb.Value.Key))
                        ret[tb.Value.Key] += tb.Value.Value;
                    else
                        ret[tb.Value.Key] = tb.Value.Value;
                }
                foreach (var tb in conditionalTagBias)
                {
                    if (ret.ContainsKey(tb.Key))
                        ret[tb.Key] += tb.Value;
                    else
                        ret[tb.Key] = tb.Value;
                }

                return ret;
            }
        }

        public List<string> CurrentTags
        {
            get
            {
                var res = Tags.ToList();
                foreach (var tb in TagBias)
                    res.Add(tb.Value.Key);
                foreach (var ctb in ConditionalTagBias)
                    res.Add(ctb.Key);
                return res.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.Trim().ToLower()).Distinct().ToList();
            }
        }

        public void Clear()
        {
            TagBias.Clear();
        }

        public void Update(Avatar avatar, AuraPersona persona)
        {
            conditionalTagBias.Clear();

            // update psyches
            var ce = persona.Memory.CurrentEmotion;
            AvatarPsyche cp = null;
            var minDist = double.MaxValue;
            foreach (var ps in avatar.Psyches)
            {
                var dist = ps.Value.Emotion.DistanceTo(ce);
                if (dist < minDist)
                {
                    minDist = dist;
                    cp = ps.Value;
                }
            }
            if (cp != null && cp.Tags != null)
            {
                foreach (var tag in cp.Tags)
                {
                    if (tag == null) continue;
                    var tg = tag.Trim().ToLower();
                    if (conditionalTagBias.ContainsKey(tg))
                        conditionalTagBias[tg]++;
                    else
                        conditionalTagBias[tg] = 1;
                }
            }

            // update plugins - precondition
            foreach(var pl in Plugins)
            {
                if (pl == null) continue;
                var tagres = pl.UpdateTagRequested(avatar, persona);
                if (tagres != null && tagres.Count() > 0)
                {
                    foreach (var tag in tagres)
                    {
                        if (tag == null) continue;
                        var tg = tag.Trim().ToLower();
                        if (conditionalTagBias.ContainsKey(tg))
                            conditionalTagBias[tg]++;
                        else
                            conditionalTagBias[tg] = 1;
                    }
                }
            }

            // update conditions
            var tags = CurrentTags;
            foreach (var cond in avatar.Conditions)
            {
                if (cond.Value == null) continue;
                if (cond.Value.Tags != null && 
                    cond.Value.Tags.Length > 0 && 
                    cond.Value.Matches(tags)
                    )
                {
                    foreach (var tag in cond.Value.Tags)
                    {
                        if (tag == null) continue;
                        var tg = tag.Trim().ToLower();
                        if (conditionalTagBias.ContainsKey(tg))
                            conditionalTagBias[tg]++;
                        else
                            conditionalTagBias[tg] = 1;
                    }
                }
            }

            // update plugins - postcondition
            foreach (var pl in Plugins)
            {
                if (pl == null) continue;
                var tagres = pl.FinalizeTagRequested(avatar, persona);
                if (tagres != null && tagres.Count() > 0)
                {
                    foreach (var tag in tagres)
                    {
                        if (tag == null) continue;
                        var tg = tag.Trim().ToLower();
                        if (conditionalTagBias.ContainsKey(tg))
                            conditionalTagBias[tg]++;
                        else
                            conditionalTagBias[tg] = 1;
                    }
                }
            }
        }
    }
}
