﻿using Aura;
using Aura.Graphic;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuraCore.Persona
{
    public class AuraPersona
    {
        public List<IAuraMessageProviderPlugin> MessagePlugins { get; } = new List<IAuraMessageProviderPlugin>();

        public Memory Memory { get; } = new Memory();

        public AuraPersonaState State { get; } = new AuraPersonaState();

        public string GetRandomIdleMessage(Avatar avatar)
        {
            var col = MessagePlugins.SelectMany(x => x.MessageRequested(avatar, this, "idle", CultureInfo.CurrentCulture.TwoLetterISOLanguageName)).ToList();
            if (col.Count > 0)
            {
                var rand = new Random();
                var r = rand.Next(col.Count);
                return col[r];
            }
            return null;
        }
    }
}
