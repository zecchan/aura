﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuraCore.Persona
{
    /// <summary>
    /// Memory key (id) should be in format of
    /// (action[.subclass[.subclass...]]):(subject).
    /// For example: touch.gentle:head
    /// </summary>
    public class Memory
    {
        /// <summary>
        /// Currently active memories. This makes the base value of current psyche.
        /// </summary>
        public List<MemoryValue> ActivatedMemories { get; } = new List<MemoryValue>();

        /// <summary>
        /// Long-term stored memories. This affect psyche a little bit.
        /// </summary>
        public Dictionary<string, MemoryValue> StoredMemories { get; } = new Dictionary<string, MemoryValue>();

        /// <summary>
        /// When the avatar is becoming too negative, the most negative memories are stored here.
        /// Basically this is the coping mechanism.
        /// </summary>
        public Dictionary<string, MemoryValue> RepressedMemories { get; } = new Dictionary<string, MemoryValue>();

        public Emotion ActivateMemory(string fullAction, Emotion emotion, double value, double importance = 1)
        {
            if (string.IsNullOrWhiteSpace(fullAction)) throw new ArgumentNullException(nameof(fullAction));
            fullAction = fullAction?.ToLower().Trim();
            importance = Math.Max(0, Math.Min(1, importance));
            var mv = new MemoryValue()
            {
                MemoryId = fullAction,
                Age = 0,
                Emotion = emotion, // current emotion will affect new memories
                Importance = importance,
                Value = value,
                InitialValue = value,
                IsFresh = true
            };
            if (StoredMemories.ContainsKey(fullAction))
                mv.Emotion = mv.Emotion ^ StoredMemories[fullAction].WeightedStoredEmotion;
            if (RepressedMemories.ContainsKey(fullAction))
                mv.Emotion = mv.Emotion + RepressedMemories[fullAction].WeightedRepressedEmotion;
            ActivatedMemories.Add(mv);
            while (ActivatedMemories.Count > 20)
                ActivatedMemories.RemoveAt(0);
            return mv.Emotion;
        }

        public Emotion CurrentEmotion
        {
            get
            {
                Emotion sum = Emotion.Neutral;
                foreach (var am in ActivatedMemories)
                {
                    var ml = am.Value / Math.Max(1, am.InitialValue);
                    sum += am.Emotion * ml;
                }

                // also add 30% of stored memories
                if (StoredMemories.Count > 0)
                {
                    Emotion ssum = Emotion.Neutral;
                    foreach (var sm in StoredMemories)
                    {
                        ssum = ssum + sm.Value.WeightedStoredEmotion;
                    }
                    sum += ssum / StoredMemories.Count * 0.3;
                }

                return sum.Clamp(-200, 200);
            }
        }

        public Emotion SentimentToward(string action, params string[] subclass)
        {
            Emotion baseSentiment = SentimentToward(action);
            int cnt = 1;

            if (subclass != null && subclass.Length > 0)
            {
                var path = action;
                foreach (var sub in subclass)
                {
                    path += "." + sub;
                    baseSentiment += sentimentToward(path);
                    cnt++;
                }
            }

            return baseSentiment / cnt;
        }

        private Emotion sentimentToward(string fullAction)
        {
            throw new NotImplementedException();
        }

        public void Update(double deltaSecond)
        {
            // Active Memory
            for (var i = 0; i < ActivatedMemories.Count; i++)
            {
                var mv = ActivatedMemories[i];
                mv.Importance = Math.Max(0, Math.Min(1, mv.Importance));
                mv.Value -= deltaSecond * (2 - mv.Importance);
                mv.Age = 0;
                ActivatedMemories[i] = mv;
            }
            ActivatedMemories.RemoveAll(x => x.Value <= 0);

            // push to stored memory (train stored memories)
            var curEmot = CurrentEmotion;
            if (curEmot.Strength >= 100) // record on high emotion
            {
                for (var i = 0; i < ActivatedMemories.Count; i++)
                {
                    var mv = ActivatedMemories[i];
                    if (!mv.IsFresh) continue;
                    // only records fresh memories
                    mv.IsFresh = false;
                    ActivatedMemories[i] = mv;
                    if (StoredMemories.ContainsKey(mv.MemoryId))
                    {
                        var csm = StoredMemories[mv.MemoryId];
                        csm.Importance = (csm.Importance + mv.Importance) / 2;
                        csm.Emotion = (csm.Emotion + (curEmot / 100)).Clamp(-50, 50);
                        csm.Age = 1;
                        StoredMemories[mv.MemoryId] = csm;
                    }
                    else
                    {
                        var sm = new MemoryValue()
                        {
                            MemoryId = mv.MemoryId,
                            Importance = mv.Importance,
                            Emotion = (curEmot / 100).Clamp(-50, 50),
                            Age = 1,
                            InitialValue = 0,
                            Value = 0,
                            IsFresh = false
                        };
                        StoredMemories[mv.MemoryId] = sm;
                    }
                }
            }

            // add age to stored memory
            var SMKeys = StoredMemories.Keys.ToList();
            foreach (var smk in SMKeys)
            {
                var mv = StoredMemories[smk];
                mv.Age += deltaSecond / 10000;
                StoredMemories[smk] = mv;
            }
        }
    }

    public struct MemoryValue
    {
        public string MemoryId;

        /// <summary>
        /// The emotion that is associated with this memory.
        /// </summary>
        public Emotion Emotion;

        /// <summary>
        /// The importance of this memory, this must range from 0 to 1 (auto-clamped). This affects decay rate, the more important this memory is, the lower the decay rate.
        /// </summary>
        public double Importance;

        /// <summary>
        /// When this value reaches 0, the memory is forgotten if in activated memory space. This does not affect stored memories.
        /// </summary>
        public double Value;

        /// <summary>
        /// This is the initial value when the memory is first activated.
        /// </summary>
        public double InitialValue;

        /// <summary>
        /// The age of this memory. Activated memory always at 0. Increases by 1 for each 10000 seconds. 
        /// </summary>
        public double Age;

        /// <summary>
        /// Whether this memory is fresh and not pushed yet to stored memories.
        /// </summary>
        public bool IsFresh;

        public Emotion WeightedStoredEmotion { get => Age < 1 ? Emotion.Neutral : Emotion / Age; }

        public Emotion WeightedRepressedEmotion { get => Emotion * Emotion.Strength / 100; }

        /// <summary>
        /// The strength of this memory.
        /// </summary>
        public double Strength { get => Emotion.Strength * Math.Max(0, Math.Min(1, Importance)); }
    }

    public class MemoryValueConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(MemoryValue) == objectType;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            MemoryValue val = new MemoryValue();
            if (typeof(MemoryValue) == objectType)
            {
                while (reader.Read())
                {
                    if (reader.TokenType != JsonToken.PropertyName)
                        break;

                    var propName = (string)reader.Value;
                    if (!reader.Read())
                        continue;
                    if (propName == "Age")
                        val.Age = serializer.Deserialize<double>(reader);
                    if (propName == "Emotion")
                        val.Emotion = serializer.Deserialize<Emotion>(reader);
                    if (propName == "Importance")
                        val.Importance = serializer.Deserialize<double>(reader);
                    if (propName == "InitialValue")
                        val.InitialValue = serializer.Deserialize<double>(reader);
                    if (propName == "IsFresh")
                        val.IsFresh = serializer.Deserialize<bool>(reader);
                    if (propName == "MemoryId")
                        val.MemoryId = serializer.Deserialize<string>(reader);
                    if (propName == "Value")
                        val.Value = serializer.Deserialize<double>(reader);
                }
            }
            return val;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is MemoryValue mv)
            {
                writer.WriteStartObject();

                writer.WritePropertyName("Age");
                serializer.Serialize(writer, mv.Age);
                writer.WritePropertyName("Emotion");
                serializer.Serialize(writer, mv.Emotion);
                writer.WritePropertyName("Importance");
                serializer.Serialize(writer, mv.Importance);
                writer.WritePropertyName("InitialValue");
                serializer.Serialize(writer, mv.InitialValue);
                writer.WritePropertyName("IsFresh");
                serializer.Serialize(writer, mv.IsFresh);
                writer.WritePropertyName("MemoryId");
                serializer.Serialize(writer, mv.MemoryId);
                writer.WritePropertyName("Value");
                serializer.Serialize(writer, mv.Value);

                writer.WriteEndObject();
            }
        }
    }

    public class EmotionConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(Emotion) == objectType;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (typeof(Emotion) == objectType)
            {
                Emotion val = new Emotion();
                if (typeof(Emotion) == objectType)
                {
                    while (reader.Read())
                    {
                        if (reader.TokenType != JsonToken.PropertyName)
                            break;

                        var propName = (string)reader.Value;
                        if (!reader.Read())
                            continue;
                        if (propName == "Alertness")
                            val.Alertness = serializer.Deserialize<double>(reader);
                        if (propName == "Pleasure")
                            val.Pleasure = serializer.Deserialize<double>(reader);
                    }
                }
                return val;
            }
            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is Emotion em)
            {
                writer.WriteStartObject();

                writer.WritePropertyName("Alertness");
                serializer.Serialize(writer, em.Alertness);
                writer.WritePropertyName("Pleasure");
                serializer.Serialize(writer, em.Pleasure);

                writer.WriteEndObject();
            }
        }
    }
}
