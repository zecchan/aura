﻿using Aura.Graphic;
using AuraCore.Persona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura
{
    public interface IAuraPlugin
    {
        void SaveState();

        void LoadState();

        void Shutdown();
    }

    public interface IAuraStatePlugin: IAuraPlugin
    {
        /// <summary>
        /// This is called after psyche tags are assigned and before condition tags.
        /// </summary>
        IEnumerable<string> UpdateTagRequested(Avatar avatar, AuraPersona persona);
        /// <summary>
        /// This is called after all tag updaters are called.
        /// </summary>
        IEnumerable<string> FinalizeTagRequested(Avatar avatar, AuraPersona persona);
    }

    public interface IAuraMessageProviderPlugin : IAuraPlugin
    {
        IEnumerable<string> MessageRequested(Avatar avatar, AuraPersona persona, string eventId, string localeId);
    }

    public interface IAuraEventHandlerPlugin: IAuraPlugin
    {
        void HandleEvent(Avatar avatar, AuraPersona persona, string eventId, object eventData, EventDispatchedEventArgument args);
    }

    public interface IAuraServicePlugin : IAuraPlugin
    {

    }

    public interface IAuraWidgetPlugin : IAuraPlugin
    {
        void Show(Point point);

        Guid Guid { get; }

        void Hide();
    }
}
