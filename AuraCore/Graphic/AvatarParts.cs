﻿using AuraCore.Persona;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.Graphic
{
    public class Avatar
    {
        public AvatarDrawables Drawables { get; set; }
        public Dictionary<string, AvatarPsyche> Psyches { get; } = new Dictionary<string, AvatarPsyche>();
        public Dictionary<string, AvatarCondition> Conditions { get; } = new Dictionary<string, AvatarCondition>();
        public Dictionary<string, AvatarRegion[]> Regions { get; } = new Dictionary<string, AvatarRegion[]>();

        Dictionary<string, string> currentState { get; } = new Dictionary<string, string>();
        /// <summary>
        /// Gets the current state of avatar display
        /// </summary>
        [JsonIgnore]
        public Dictionary<string, string> CurrentState { get => currentState.ToDictionary(x => x.Key, x => x.Value); }

        public void UpdateState(AuraPersona persona)
        {
            currentState.Clear();
            foreach (var kv in Drawables.LayerDefinitions)
            {
                currentState.Add(kv.Key, kv.Value.Default);
            }

            // Calculate tags and assign drawable states
            var tagPowers = persona.State.CurrentTagPower;

            SetStateByTagPower(Drawables.Sets.Hairs, tagPowers);
            SetStateByTagPower(Drawables.Sets.Outfits, tagPowers);
            SetStateByTagPower(Drawables.Sets.Expressions, tagPowers);

            OnUpdate?.Invoke(this, null);
        }

        private void SetStateByTagPower(Dictionary<string, AvatarSetDefinition> set, Dictionary<string, int> tagPower)
        {
            Dictionary<string, string> layer = null;
            double maxPower = 0;
            foreach (var kv in set)
            {
                if (kv.Value == null || kv.Value.Tags == null) continue;
                var power = tagPower.Where(x => kv.Value.Tags.Contains(x.Key)).Sum(x => x.Value);
                if (power > maxPower)
                {
                    layer = kv.Value.Layers;
                    maxPower = power;
                }
            }
            if (layer != null)
            {
                foreach (var kv in layer)
                {
                    if (currentState.ContainsKey(kv.Key))
                        currentState[kv.Key] = kv.Value;
                }
            }
        }

        public IEnumerable<string> GetRegions(Point point)
        {
            var res = new List<string>();
            foreach (var r in Regions)
            {
                if (r.Value != null && r.Value.Any(x => x.Contains(point)))
                    res.Add(r.Key);
            }
            return res;
        }

        public event EventHandler OnUpdate;

        public string CurrentPsyche(Emotion emotion)
        {
            if (Psyches == null) return null;
            var minDist = double.MaxValue;
            string nearest = null;
            foreach (var ps in Psyches)
            {
                var dist = ps.Value.Emotion.DistanceTo(emotion);
                if (dist < minDist)
                {
                    minDist = dist;
                    nearest = ps.Key;
                }
            }
            return nearest;
        }
    }

    public class AvatarRegion
    {
        public string Type { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double R { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }

        public bool Contains(Point point)
        {
            if (Type == "radius")
            {
                var dist = Math.Sqrt(Math.Pow(point.X - X, 2) + Math.Pow(point.Y - Y, 2));
                return dist <= R;
            }
            if (Type == "rect")
            {
                return point.X >= X && point.X <= (X + Width) && point.Y >= Y && point.Y <= (Y + Height);
            }
            return false;
        }
    }

    public class AvatarDrawables
    {
        public Dictionary<string, AvatarLayer> LayerDefinitions { get; } = new Dictionary<string, AvatarLayer>();

        public AvatarSets Sets { get; } = new AvatarSets();
    }

    public class AvatarLayer
    {
        public string Path { get; set; }

        public string Default { get; set; }
    }

    public class AvatarSets
    {
        public Dictionary<string, AvatarSetDefinition> Outfits { get; } = new Dictionary<string, AvatarSetDefinition>();
        public Dictionary<string, AvatarSetDefinition> Hairs { get; } = new Dictionary<string, AvatarSetDefinition>();
        public Dictionary<string, AvatarSetDefinition> Expressions { get; } = new Dictionary<string, AvatarSetDefinition>();
    }

    public class AvatarSetDefinition
    {
        public Dictionary<string, string> Layers { get; } = new Dictionary<string, string>();

        public string[] Tags { get; set; }
    }

    public class AvatarPsyche
    {
        public Emotion Emotion { get; set; }

        public string[] Tags { get; set; }
    }

    public class AvatarCondition : AvatarTagConditionBase
    {
        public string[] Tags { get; set; }

        public AvatarConditionTimeSlot[] TimeSlots { get; set; }

        public override bool Matches(IEnumerable<string> tags)
        {
            if (!base.Matches(tags)) return false;
            if (TimeSlots == null || TimeSlots.Length == 0) return true;
            if (TimeSlots.Any(x => x.Matches(tags)))
                return true;
            return false;
        }
    }

    public class AvatarConditionTimeSlot : AvatarTagConditionBase
    {
        public TimeSpan? From { get; set; }
        public TimeSpan? To { get; set; }

        public string[] Dow { get; set; }

        public override bool Matches(IEnumerable<string> tags)
        {
            if (!base.Matches(tags)) return false;
            var now = DateTime.Now;

            var dow = now.DayOfWeek.ToString().ToLower();
            if (Dow != null && Dow.Length > 0 && !Dow.Contains(dow)) return false;

            var tod = now.TimeOfDay;
            if (From != null)
            {
                if (To != null)
                {
                    if (From.Value > To.Value)
                    {
                        if (tod > From.Value || tod < To.Value) return true;
                    }
                    else
                    {
                        if (tod > From.Value && tod < To.Value) return true;
                    }
                    return false;
                }
                else
                {
                    if (tod > From.Value) return false;
                }
            }
            else
            {
                if (To != null)
                {
                    if (tod < To.Value) return false;
                }
                else
                {
                    // no condition
                }
            }

            return true;
        }
    }

    public class AvatarTagConditionBase
    {
        /// <summary>
        /// Tags must contain all tags in this array
        /// </summary>
        public string[] MatchAll { get; set; }
        /// <summary>
        /// Tags must not contain all tags in this array, if only some of them matched, then it is valid.
        /// </summary>
        public string[] NotMatchAll { get; set; }
        /// <summary>
        /// Tags must contain any tag in this array
        /// </summary>
        public string[] MatchAny { get; set; }
        /// <summary>
        /// Tags must not contain any tag in this array
        /// </summary>
        public string[] NotMatchAny { get; set; }

        public virtual bool Matches(IEnumerable<string> tags)
        {
            if (tags == null) return false;
            if (MatchAll != null && MatchAll.Length > 0 && !MatchAll.All(x => tags.Contains(x)))
                return false;
            if (NotMatchAll != null && NotMatchAll.Length > 0 && !NotMatchAll.All(x => !tags.Contains(x)))
                return false;
            if (MatchAny != null && MatchAny.Length > 0 && !MatchAny.Any(x => tags.Contains(x)))
                return false;
            if (NotMatchAny != null && NotMatchAny.Length > 0 && !NotMatchAny.Any(x => !tags.Contains(x)))
                return false;
            return true;
        }
    }
}
