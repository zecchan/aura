﻿using Aura;
using BondTech.HotKeyManagement.WPF._4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AuraUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DispatcherTimer Timer { get; } = new DispatcherTimer();
        public DateTime TimerTracker { get; private set; }

        public GlobalHotKey GHKAltShiftQ = new GlobalHotKey("ghkCall", ModifierKeys.Alt | ModifierKeys.Shift, Keys.Q, true);
        public HotKeyManager HotKeyManager { get; private set; }

        public MainWindow()
        {
            InitializeComponent();
            AI.LoadConfig("config.json");
            AI.LoadAvatar("data\\" + AI.Config.Avatar + "\\avatar.json");            
            AI.LoadMemory();
            AI.LoadPlugins("plugins");
            AI.Persona.State.Clear();
            AI.Avatar.OnUpdate += Avatar_OnUpdate;
            AI.EventDispatched += AI_EventDispatched;

            Topmost = AI.Config.Topmost;

            var ld = AI.Avatar.Drawables.LayerDefinitions.ToList();
            ld.Reverse();
            foreach (var kv in ld)
            {
                var img = new Image();
                img.Tag = null;
                Grid.SetColumn(img, 0);
                Grid.SetRow(img, 0);
                img.Stretch = Stretch.Uniform;
                img.MouseDown += imgMouseDown;
                img.MouseMove += imgMouseMove;
                img.MouseUp += imgMouseUp;
                img.Cursor = System.Windows.Input.Cursors.Hand;
                img.HorizontalAlignment = HorizontalAlignment.Center;
                img.VerticalAlignment = VerticalAlignment.Center;
                img.AllowDrop = true;
                img.DragEnter += Img_DragEnter;
                img.DragOver += Img_DragOver;
                img.Drop += Img_Drop;
                grDisplay.Children.Add(img);
                AvatarLayers.Add(kv.Key, img);
            }

            AI.Persona.State.Update(AI.Avatar, AI.Persona);

            if (AI.Config.WindowHeight != null && AI.Config.WindowWidth != null)
            {
                Height = AI.Config.WindowHeight.Value;
                Width = AI.Config.WindowWidth.Value;
            }
            else
            {
                var imgSize = AvatarLayers.Where(x => x.Value.Source is BitmapImage).Select(x => new Size((x.Value.Source as BitmapImage).PixelWidth, (x.Value.Source as BitmapImage).PixelHeight)).FirstOrDefault();
                if (imgSize.Height > 0 && imgSize.Width > 0)
                {
                    Height = imgSize.Height;
                    Width = imgSize.Width;
                }
            }

            Timer.Tick += timerTick;
            Timer.Interval = new TimeSpan(0, 0, 0, 0, 50);
            TimerTracker = DateTime.Now;
            Timer.Start();

            AI.DataRequested += AI_DataRequested;
        }

        private void Img_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var data = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (data != null)
                {
                    AI.DispatchEvent("input.filedrop", data);
                }
            }
        }

        private void Img_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
            }
        }

        private void Img_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
            }
        }

        private object AI_DataRequested(string dataId, Type preferedDataType)
        {
            if (dataId == "window.state" && typeof(AvatarWindowStateArgument) == preferedDataType)
            {
                return MakeAvatarWindowStateArgument();
            }
            if (dataId != null && dataId.StartsWith("avatar.layervisibility:"))
            {
                var lid = dataId.Substring("avatar.layervisibility:".Length);
                if (AvatarLayers.ContainsKey(lid))
                {
                    return AvatarLayers[lid].Visibility == Visibility.Visible;
                }
            }
            return null;
        }

        public void BringToFront()
        {
            var tm = Topmost;
            if (!IsVisible)
                Show();
            if (WindowState == WindowState.Minimized)
                WindowState = WindowState.Normal;

            Topmost = true;
            Activate();
            if (!tm)
                Topmost = false;
            Focus();
        }

        private void HotKeyManager_GlobalHotKeyPressed(object sender, GlobalHotKeyEventArgs e)
        {
            if (!IsActive || Topmost)
            {
                if (!Topmost)
                {
                    grMain.Opacity = 0;
                    var s = new Storyboard();

                    DoubleAnimation da = new DoubleAnimation()
                    {
                        From = 0.5,
                        To = 1,
                        Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300)),
                        AutoReverse = false
                    };
                    s.Children.Add(da);
                    s.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300));
                    Storyboard.SetTarget(da, grMain);
                    Storyboard.SetTargetProperty(da, new PropertyPath("(Grid.Opacity)"));
                    s.Begin();
                    BringToFront();
                }
                else
                {
                    Activate();
                    Focus();
                }
            }
        }

        bool MuteActiveEvent { get; set; }
        private void AI_EventDispatched(string eventId, object eventData, EventDispatchedEventArgument e)
        {
            if (eventId == "console.log" && eventData is string text)
            {
                ConsoleLog(text);
            }

            if (eventId == "window.requestactive")
            {
                if (eventData is bool b && b == false)
                {
                    MuteActiveEvent = true;
                    Activate();
                    MuteActiveEvent = false;
                }
                else
                {
                    Activate();
                }
            }

            if (eventId == "shutdown")
            {
                Close();
            }

            if (eventId == "avatar.hidelayer" && eventData is string hideLayerId)
            {
                if (AvatarLayers.ContainsKey(hideLayerId))
                    AvatarLayers[hideLayerId].Visibility = Visibility.Collapsed;
            }
            if (eventId == "avatar.showlayer" && eventData is string showLayerId)
            {
                if (AvatarLayers.ContainsKey(showLayerId))
                    AvatarLayers[showLayerId].Visibility = Visibility.Visible;
            }
        }

        private void ConsoleLog(string text)
        {
            var sbConsole = new Storyboard();
            var tx = new TextBlock()
            {
                Text = text,
                Background = new SolidColorBrush(Color.FromArgb(128, 0, 0, 0)),
                Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255)),
                Padding = new Thickness(2),
                FontSize = 8,
                FontFamily = new FontFamily("Consolas")
            };
            spConsole.Children.Add(tx);
            DoubleAnimation da = new DoubleAnimation()
            {
                From = 1,
                To = 0,
                Duration = new Duration(new TimeSpan(0, 0, 3)),
                AutoReverse = false
            };
            sbConsole.Children.Add(da);
            sbConsole.Duration = new Duration(new TimeSpan(0, 0, 3));
            Storyboard.SetTarget(da, tx);
            Storyboard.SetTargetProperty(da, new PropertyPath("(TextBlock.Opacity)"));
            sbConsole.Completed += SbConsole_Completed;
            sbConsole.Begin();
        }

        private void SbConsole_Completed(object sender, EventArgs e)
        {
            if (spConsole.Children.Count > 0)
                spConsole.Children.RemoveAt(0);
        }

        private void timerTick(object sender, EventArgs e)
        {
            var delta = DateTime.Now - TimerTracker;
            TimerTracker = DateTime.Now;
            if (AI.Persona != null)
            {
                AI.Persona.Memory.Update(delta.TotalSeconds);
                AI.Persona.State.Update(AI.Avatar, AI.Persona);
            }

            AI.DispatchEvent("timer.tick", null);

            if (AI.Avatar != null)
                AI.Avatar.UpdateState(AI.Persona);
        }

        private void Avatar_OnUpdate(object sender, EventArgs e)
        {
            Dispatcher.Invoke(new Action(UpdateAvatar));
        }

        bool _activated = false;
        private void Window_Activated(object sender, EventArgs e)
        {
            if (!MuteActiveEvent)
                SendWindowState("window.activated");
            if (_activated) return;
            _activated = true;
            var screen = System.Windows.Forms.Screen.FromPoint(new System.Drawing.Point((int)Left, (int)Top));
            Left = screen.WorkingArea.Right - Width;
            Top = screen.WorkingArea.Bottom - Height;

            HotKeyManager = new HotKeyManager(this);
            HotKeyManager.AddGlobalHotKey(GHKAltShiftQ);

            HotKeyManager.GlobalHotKeyPressed += HotKeyManager_GlobalHotKeyPressed;

            if (AI.Config.Left != null && AI.Config.Top != null)
            {
                Left = AI.Config.Left.Value;
                Top = AI.Config.Top.Value;
            }

            SendWindowState("window.activatedonce");
        }

        Dictionary<string, Image> AvatarLayers { get; } = new Dictionary<string, Image>();
        private void UpdateAvatar()
        {
            var cs = AI.Avatar.CurrentState;
            foreach (var kv in cs)
            {
                if (string.IsNullOrWhiteSpace(kv.Value))
                {
                    if (AvatarLayers.ContainsKey(kv.Key))
                    {
                        AvatarLayers[kv.Key].Source = null;
                        AvatarLayers[kv.Key].Tag = null;
                    }
                    continue;
                }
                if (AvatarLayers.ContainsKey(kv.Key))
                {
                    var fpath = System.IO.Path.Combine(AI.Avatar.Drawables.LayerDefinitions[kv.Key].Path, kv.Value);
                    var img = AvatarLayers[kv.Key];
                    if (img.Tag is string src && src == fpath) continue;
                    img.Tag = fpath;
                    try
                    {
                        if (System.IO.File.Exists(fpath))
                            img.Source = new BitmapImage(new Uri(System.IO.Path.GetFullPath(fpath)));
                    }
                    catch { }
                }
            }
            AI.UIReady = true;
        }

        bool mouseIsDownOnDragger { get; set; }
        Point windowInitialLocation { get; set; }
        DateTime windowDragStart { get; set; }
        Point mouseInitialPointOnDragger { get; set; }
        UIElement mouseCapturer { get; set; }

        private void SendWindowState(string eventId)
        {
            var warg = MakeAvatarWindowStateArgument();
            if (warg != null)
                AI.DispatchEvent(eventId, warg);
        }

        public AvatarWindowStateArgument MakeAvatarWindowStateArgument()
        {
            Image img = null;
            foreach (var ch in grDisplay.Children)
            {
                if (ch is Image im && im.Source != null)
                    img = im;
            }
            if (img != null)
            {
                return new AvatarWindowStateArgument()
                {
                    WindowLocation = new Point(Left, Top),
                    AvatarLocation = new Point((Width - img.ActualWidth) / 2, (Height - img.ActualHeight) / 2),
                    AvatarSize = new Size(img.ActualWidth, img.ActualHeight),
                    WindowSize = new Size(Width, Height)
                };
            }
            return null;
        }

        private void imgMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && sender is UIElement ele)
            {
                mouseInitialPointOnDragger = e.GetPosition(ele);
                mouseIsDownOnDragger = true;
                ele.CaptureMouse();
                mouseCapturer = ele;
                windowInitialLocation = new Point(Left, Top);
                windowDragStart = DateTime.Now;
                e.Handled = true;
            }
        }

        private void imgMouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseIsDownOnDragger) return;
            if (sender is UIElement ele && ele == mouseCapturer)
            {
                var npos = e.GetPosition(ele);
                var dx = npos.X - mouseInitialPointOnDragger.X;
                var dy = npos.Y - mouseInitialPointOnDragger.Y;
                var l = Left + dx;
                var t = Top + dy;

                // restrict in visible area
                var screen = System.Windows.Forms.Screen.FromPoint(System.Windows.Forms.Cursor.Position);

                if (l + Width > screen.WorkingArea.Right)
                    l = screen.WorkingArea.Right - Width;
                if (t + Height > screen.WorkingArea.Bottom)
                    t = screen.WorkingArea.Bottom - Height;
                if (l < screen.WorkingArea.Left)
                    l = screen.WorkingArea.Left;
                if (t < screen.WorkingArea.Top)
                    t = screen.WorkingArea.Top;
                Left = l;
                Top = t;

                SendWindowState("window.moving");

                e.Handled = true;
            }
        }

        private void imgMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && mouseIsDownOnDragger)
            {
                mouseIsDownOnDragger = false;
                if (mouseCapturer != null)
                {
                    mouseCapturer.ReleaseMouseCapture();
                    mouseCapturer = null;
                }
                e.Handled = true;
                if ((DateTime.Now - windowDragStart).TotalMilliseconds < 300 && sender is Image img)
                {
                    var clog = new Point(Left, Top);
                    var dist = Math.Sqrt(Math.Pow(clog.X - windowInitialLocation.X, 2) + Math.Pow(clog.Y - windowInitialLocation.Y, 2));
                    if (dist < 20)
                    {
                        // click on avatar, now we want to know at which point
                        var pt = e.GetPosition(img);
                        var px = pt.X / img.ActualWidth * 100;
                        var py = pt.Y / img.ActualHeight * 100;
                        var cpt = new Point(px, py);
                        var arg = new AvatarClickEventArguments()
                        {
                            ClickAvatarPercentagePoint = cpt,
                            RegionIds = AI.Avatar.GetRegions(cpt).ToArray(),
                            ClickWindowAbsolutePoint = e.GetPosition(this),
                            WindowLocation = new Point(Left, Top),
                            AvatarLocation = new Point((Width - img.ActualWidth) / 2, (Height - img.ActualHeight) / 2),
                            AvatarSize = new Size(img.ActualWidth, img.ActualHeight),
                            ClickAvatarAbsolutePoint = e.GetPosition(img),
                            ClickScreenAbsolutePoint = img.PointToScreen(e.GetPosition(img)),
                            WindowSize = new Size(Width, Height)
                        };
                        AI.DispatchEvent("avatar.click", arg);
                        return;
                    }
                }
                AI.Config.Left = Left;
                AI.Config.Top = Top;
                SendWindowState("window.moved");
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AI.Config.WindowWidth = Width;
            AI.Config.WindowHeight = Height;
            SendWindowState("window.sizechanged");
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                Timer.Stop();
                AI.Shutdown();
            }
            catch { }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.T)
            {
                Topmost = !Topmost;
                ConsoleLog(Topmost ? "Always on Top: ON" : "Always on Top: OFF");

                AI.Config.Topmost = true;
                e.Handled = true;
                return;
            }
            if (e.Key == Key.F5)
            {
                ConsoleLog("Refreshing avatar metadata");
#if DEBUG
                var path = "../../data/" + AI.Config.Avatar + "/avatar.json";
#else
                var path = null;
#endif
                AI.ReloadAvatar(path);
                e.Handled = true;
                return;
            }
            AI.DispatchEvent("window.keydown", e);
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.T)
            {
                e.Handled = true;
                return;
            }
            AI.DispatchEvent("window.keyup", e);
        }

    }
}
