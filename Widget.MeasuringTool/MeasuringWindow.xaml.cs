﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Widget.MeasuringTool
{
    /// <summary>
    /// Interaction logic for MeasuringWindow.xaml
    /// </summary>
    public partial class MeasuringWindow : Window
    {
        public DispatcherTimer Timer { get; } = new DispatcherTimer();
        public MeasuringWindow()
        {
            InitializeComponent();
        }

        bool IsClosing = false;

        private void S_Completed(object sender, EventArgs e)
        {
            Close();
        }

        public void RequestClose(object sender)
        {
            if (IsClosing) return;
            IsClosing = true;

            var s = new Storyboard();

            DoubleAnimation da = new DoubleAnimation()
            {
                From = 1,
                To = 0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 600)),
                AutoReverse = false
            };
            s.Children.Add(da);
            s.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 600));
            Storyboard.SetTarget(da, grMain);
            Storyboard.SetTargetProperty(da, new PropertyPath("(Grid.Opacity)"));

            s.Completed += S_Completed;
            s.Begin();
        }

        private void grMain_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RequestClose(sender);
        }
    }
}
