﻿using Aura.Graphic;
using AuraCore.Persona;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Aura.Main
{
    public class AuraMainPlugin : IAuraEventHandlerPlugin, IAuraMessageProviderPlugin
    {
        public SpeechBubble SpeechBubble { get; private set; }

        public NetworkMonitor NetMon { get; } = new NetworkMonitor();

        public Config Config { get; private set; }

        public AuraMainPlugin()
        {
            NetMon.Update();
            NetMon.OnInternetIsUpChanged += NetMon_OnInternetIsUpChanged;
        }

        bool netEverDown = false;
        private void NetMon_OnInternetIsUpChanged(object sender, EventArgs e)
        {
            if (!AI.UIReady || Config == null || !Config.MonitorNetwork) return;
            if (NetMon.InternetIsUp)
            {
                if (netEverDown)
                {
                    ShowBubble("Your internet is back on!", null);
                    netEverDown = false;
                }
            }
            else
            {
                netEverDown = true;
                ShowBubble("It seems that your internet is down.", null);
            }
        }

        public AuraMenu ControlPanel { get; internal set; }

        public DateTime LastCPUpdate { get; set; } = DateTime.Now;

        public void HandleEvent(Avatar avatar, AuraPersona persona, string eventId, object eventData, EventDispatchedEventArgument args)
        {
            if (eventId == "avatar.click" && eventData is AvatarClickEventArguments acea)
            {
                var memoryPrefix = "touch:";
                string say = null;
                if (acea.RegionIds != null && acea.RegionIds.Length > 0)
                {
                    foreach (var region in acea.RegionIds)
                    {
                        var path = memoryPrefix + region;

                        persona.Memory.ActivateMemory(path,
                            region == "face" ? new Emotion() { Alertness = EmotionValues.MediumCalm, Pleasure = EmotionValues.LowPleasure } :
                            region == "boobs" ? new Emotion() { Alertness = EmotionValues.MediumAlert, Pleasure = EmotionValues.MediumDispleasure } :
                            region == "pussy" ? new Emotion() { Alertness = EmotionValues.MediumAlert, Pleasure = EmotionValues.HighDispleasure } :
                            Emotion.Neutral
                            , 3.5);
                    }
                    var ce = persona.Memory.CurrentEmotion;
                    var psy = avatar.CurrentPsyche(ce)?.ToLower().Trim();
                    if (psy == "shame")
                        say = "Please stop touching my " + acea.RegionIds[0] + "...";
                    if (psy == "disgust")
                        say = "Stop touching me!";

                    var randMess = persona.GetRandomIdleMessage(avatar);
                }

                if (SpeechBubble.MessageIsHidingOrHidden)
                {
                    if (!string.IsNullOrWhiteSpace(say))
                        ShowBubble(say, acea);
                }
                else
                {
                    SpeechBubble.HideMessage();
                }
            }

            if (eventData is AvatarWindowStateArgument awsa)
            {
                if (
                    eventId == "window.moving" ||
                    eventId == "window.activatedonce" ||
                    eventId == "window.sizechanged"
                    )
                    SnapSpeechBubble(awsa);
            }

            if (eventId == "window.activated")
            {
                if (SpeechBubble != null && SpeechBubble.IsVisible)
                {
                    SpeechBubble.Topmost = true;
                    SpeechBubble.Topmost = false;
                }
            }

            if (eventId == "timer.tick")
            {
                if (Config != null && Config.MonitorNetwork)
                    NetMon.Update();
                if (ControlPanel != null && (DateTime.Now - LastCPUpdate).TotalMilliseconds > 200)
                {
                    LastCPUpdate = DateTime.Now;
                    var ce = persona.Memory.CurrentEmotion;
                    var x = ce.Pleasure + (ControlPanel.grEmoPos.ActualWidth - ControlPanel.elEmoPos.ActualWidth) / 2;
                    var y = -ce.Alertness + (ControlPanel.grEmoPos.ActualHeight - ControlPanel.elEmoPos.ActualHeight) / 2;
                    ControlPanel.elEmoPos.Margin = new System.Windows.Thickness()
                    {
                        Left = Math.Max(Math.Min(x, ControlPanel.grEmoPos.ActualWidth - ControlPanel.elEmoPos.ActualWidth / 2), -ControlPanel.elEmoPos.ActualWidth / 2),
                        Top = Math.Max(Math.Min(y, ControlPanel.grEmoPos.ActualHeight - ControlPanel.elEmoPos.ActualHeight / 2), -ControlPanel.elEmoPos.ActualHeight / 2)
                    };
                    ControlPanel.txCurEmo.Text = avatar.CurrentPsyche(ce);
                    ControlPanel.txCurAlert.Text = Math.Round(ce.Alertness, 6).ToString();
                    ControlPanel.txCurPleasant.Text = Math.Round(ce.Pleasure, 6).ToString();
                    ControlPanel.txCurActive.Text = persona.Memory.ActivatedMemories.Count.ToString();
                    ControlPanel.txCurStored.Text = persona.Memory.StoredMemories.Count.ToString();
                    ControlPanel.txCurStrength.Text = Math.Round(ce.Strength, 6).ToString();
                    ControlPanel.txCurTag.Text = string.Join(", ", persona.State.CurrentTagPower.Select(w => w.Key + " (" + w.Value + ")"));

                    if (persona.Memory.ActivatedMemories.Count > 0)
                    {
                        var lm = persona.Memory.ActivatedMemories.Last();
                        ControlPanel.txLastMem.Text = lm.MemoryId;
                        ControlPanel.txLastMemActive.Text = "(P: " + Math.Round(lm.Emotion.Pleasure, 6) + ", A: " + Math.Round(lm.Emotion.Alertness) + ")";
                        if (persona.Memory.StoredMemories.ContainsKey(lm.MemoryId))
                        {
                            var sm = persona.Memory.StoredMemories[lm.MemoryId];
                            ControlPanel.txLastMemStored.Text = "(P: " + Math.Round(sm.Emotion.Pleasure, 6) + ", A: " + Math.Round(sm.Emotion.Alertness) + ")";
                        }
                        else
                            ControlPanel.txLastMemStored.Text = "-";
                    }
                    else
                    {
                        ControlPanel.txLastMem.Text = "-";
                        ControlPanel.txLastMemActive.Text = "-";
                        ControlPanel.txLastMemStored.Text = "-";
                    }
                }
            }

            if (eventId == "window.keydown" && eventData is System.Windows.Input.KeyEventArgs ke)
            {
                if (ke.Key == Key.A)
                {
                    if (ControlPanel == null)
                    {
                        ControlPanel = new AuraMenu();
                        ControlPanel.SourcePlugin = this;
                        ControlPanel.Prepare();
                        ControlPanel.Show();
                        var awsa2 = AI.RequestData<AvatarWindowStateArgument>("window.state").FirstOrDefault();
                        if (awsa2 != null)
                        {
                            ControlPanel.Left = awsa2.AbsoluteAvatarLocation.X - ControlPanel.Width;
                            ControlPanel.Top = awsa2.AbsoluteAvatarLocation.Y - ControlPanel.Height;
                        }
                    }
                }
                if (ke.Key == Key.N)
                {
                    Config.MonitorNetwork = !Config.MonitorNetwork;
                    SaveState();
                    AI.DispatchEvent("console.log", Config.MonitorNetwork ? "Network Monitor is ON" : "Network Monitor is OFF");
                }
                if (ke.Key == Key.Escape)
                {
                    if (MessageBox.Show("Exit Aura?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        AI.Shutdown();
                }
            }

            if (eventId == "input.filedrop" && eventData is string[] paths)
            {
                if (paths.Length == 0) return;
                if (paths.Length == 1)
                {
                    var path = paths.First();
                    if (File.Exists(path))
                    {

                    }
                    else
                    {

                    }
                }
                else
                {
                    var files = paths.Where(x => File.Exists(x)).ToList();
                    var dirs = paths.Where(x => Directory.Exists(x)).ToList();
                }
            }
        }

        public void ShowBubble(string text, AvatarWindowStateArgument awsa)
        {
            if (SpeechBubble == null) return;
            SpeechBubble.ShowMessage(text);
            awsa = awsa ?? AI.RequestData<AvatarWindowStateArgument>("window.state").FirstOrDefault();
            if (awsa != null)
                SnapSpeechBubble(awsa);
        }

        public void SnapSpeechBubble(AvatarWindowStateArgument awsa)
        {
            // define quadrant
            var scrn = Screen.FromPoint(new System.Drawing.Point((int)awsa.WindowLocation.X, (int)awsa.WindowLocation.Y));
            var onLeft = scrn.Bounds.Left + scrn.Bounds.Width / 2 >= awsa.WindowLocation.X + awsa.WindowSize.Width / 2;
            var onTop = scrn.Bounds.Top + scrn.Bounds.Height / 2 >= awsa.WindowLocation.Y + awsa.WindowSize.Height / 2;

            SpeechBubble.elBig.HorizontalAlignment = onLeft ? System.Windows.HorizontalAlignment.Left : System.Windows.HorizontalAlignment.Right;
            SpeechBubble.elBig.VerticalAlignment = onTop ? System.Windows.VerticalAlignment.Top : System.Windows.VerticalAlignment.Bottom;
            SpeechBubble.elSmall.HorizontalAlignment = onLeft ? System.Windows.HorizontalAlignment.Left : System.Windows.HorizontalAlignment.Right;
            SpeechBubble.elSmall.VerticalAlignment = onTop ? System.Windows.VerticalAlignment.Top : System.Windows.VerticalAlignment.Bottom;

            if (onTop && onLeft)
            {
                SpeechBubble.Left = awsa.AbsoluteAvatarLocation.X + awsa.WindowSize.Width;
                SpeechBubble.Top = awsa.AbsoluteAvatarLocation.Y + awsa.AvatarSize.Height * 0.8;
            }
            if (!onTop && onLeft)
            {
                SpeechBubble.Left = awsa.AbsoluteAvatarLocation.X + awsa.WindowSize.Width;
                SpeechBubble.Top = awsa.AbsoluteAvatarLocation.Y - awsa.AvatarSize.Height * 0.2;
            }
            if (onTop && !onLeft)
            {
                SpeechBubble.Left = awsa.AbsoluteAvatarLocation.X - SpeechBubble.Width;
                SpeechBubble.Top = awsa.AbsoluteAvatarLocation.Y + awsa.AvatarSize.Height * 0.8;
            }
            if (!onTop && !onLeft)
            {
                SpeechBubble.Left = awsa.AbsoluteAvatarLocation.X - SpeechBubble.Width;
                SpeechBubble.Top = awsa.AbsoluteAvatarLocation.Y - awsa.AvatarSize.Height * 0.2;
            }

            AI.DispatchEvent("window.requestactive", false);
        }

        public void LoadState()
        {
            Config = AI.LoadPluginData<Config>("coatl.aura.main") ?? new Config();
            SpeechBubble = new SpeechBubble();
            SpeechBubble.Show();
            AI.DispatchEvent("window.requestactive", false);
        }

        public void SaveState()
        {
            AI.SavePluginData("coatl.aura.main", Config);
        }

        public void Shutdown()
        {
            SpeechBubble.CannotClose = false;
            SpeechBubble.Close();
            try
            {
                if (ControlPanel != null)
                    ControlPanel.Close();
            }
            catch { }
        }

        public IEnumerable<string> MessageRequested(Avatar avatar, AuraPersona persona, string eventId, string localeId)
        {
            var res = new List<string>();
            return res;
        }
    }
}
