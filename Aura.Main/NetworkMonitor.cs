﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Main
{
    public class NetworkMonitor
    {
        public DateTime LastChecked { get; private set; } = DateTime.MinValue;

        /// <summary>
        /// When to perform update in second.
        /// </summary>
        public int UpdateInterval { get; set; } = 5;

        public int LastRoundtripTime { get; set; }

        public string Host { get; set; } = "8.8.8.8";

        bool internetIsUp = false;
        public bool InternetIsUp
        {
            get => internetIsUp; 
            private set
            {
                if (internetIsUp != value)
                {
                    internetIsUp = value;
                    if (firstCheck)
                        OnInternetIsUpChanged?.Invoke(this, null);
                    else
                        firstCheck = false;
                }
            }
        }

        public async void Update()
        {
            var delta = DateTime.Now - LastChecked;
            var anyUp = false;
            var cnt = 0;
            var rtt = 0;
            if (delta.TotalSeconds >= UpdateInterval)
            {
                for(var i = 0; i < 5; i++)
                {
                    try
                    {
                        var rep = await Ping(Host);
                        if (rep.Status == IPStatus.Success)
                        {
                            anyUp = true;
                            rtt += (int)rep.RoundtripTime;
                            cnt++;
                        }
                    }
                    catch
                    {
                    }
                }

                if (cnt > 0)
                    LastRoundtripTime = rtt / cnt;
                InternetIsUp = anyUp;
                LastChecked = DateTime.Now;
            }
        }

        public async Task<PingReply> Ping(string host, int bufferLength = 32, int timeout = 500, PingOptions options = null)
        {
            if (bufferLength < 1) bufferLength = 32;
            if (timeout < 1) timeout = 1;

            options = options ?? new PingOptions()
            {
                DontFragment = true,
                Ttl = 128
            };
            var sender = new Ping();

            var data = new string('a', bufferLength);
            var buffer = Encoding.ASCII.GetBytes(data);

            return await Task.Run(new Func<PingReply>(() => sender.Send(host, timeout, buffer, options)));
        }

        public event EventHandler OnInternetIsUpChanged;

        bool firstCheck = true;
        public NetworkMonitor()
        {
        }
    }
}
