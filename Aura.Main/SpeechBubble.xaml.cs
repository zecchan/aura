﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Aura.Main
{
    /// <summary>
    /// Interaction logic for SpeechBubble.xaml
    /// </summary>
    public partial class SpeechBubble : Window
    {
        DispatcherTimer Timer { get; } = new DispatcherTimer();
        public SpeechBubble()
        {
            InitializeComponent();
            Timer.Interval = new TimeSpan(0, 0, 0, 3);
            Timer.Tick += Timer_Tick;


            DoubleAnimation da = new DoubleAnimation()
            {
                From = 1,
                To = 0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300)),
                AutoReverse = false
            };
            sbHideMessage.Children.Add(da);
            sbHideMessage.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300));
            Storyboard.SetTarget(da, grMain);
            Storyboard.SetTargetProperty(da, new PropertyPath("(Grid.Opacity)"));
            sbHideMessage.Begin();

            da = new DoubleAnimation()
            {
                From = 0.5,
                To = 1,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300)),
                AutoReverse = false
            };
            sbShowMessage.Children.Add(da);
            sbShowMessage.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300));
            Storyboard.SetTarget(da, grMain);
            Storyboard.SetTargetProperty(da, new PropertyPath("(Grid.Opacity)"));
            sbShowMessage.Completed += S_Completed1;
        }

        Storyboard sbHideMessage { get; } = new Storyboard();
        Storyboard sbShowMessage { get; } = new Storyboard();

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (MessageIsHidingOrHidden) return;
            HideMessage();
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            HideMessage();
        }

        public bool MessageIsHidingOrHidden { get; private set; } = true;

        public void HideMessage()
        {
            MessageIsHidingOrHidden = true;
            Timer.Stop();
            sbHideMessage.Begin();
            AI.DispatchEvent("window.requestactive", false);
        }

        public void ShowMessage(string text)
        {
            MessageIsHidingOrHidden = false;
            sbShowMessage.Stop();
            sbHideMessage.Stop();
            Visibility = Visibility.Hidden;
            InvalidateVisual();
            Timer.Stop();
            grMain.Opacity = 0;
            txMessage.Text = text;
            Visibility = Visibility.Visible;

            var textSize = MeasureString(text);
            var w = Math.Min(textSize.Width, 480);
            var h = Math.Ceiling(textSize.Width / 480) * textSize.Height;
            Width = Math.Min(600, w + 120);
            Height = Math.Min(400, h + 100);
            Topmost = true;
            Topmost = false;
            sbShowMessage.Begin();
        }

        private void S_Completed1(object sender, EventArgs e)
        {
            double bsDuration = 2;
            bsDuration += txMessage.Text.Length / 15.0;
            Timer.Interval = new TimeSpan(0, 0, 0, 0, (int)(bsDuration * 1000));
            Timer.Start();
        }
        private Size MeasureString(string candidate)
        {
            var formattedText = new FormattedText(
                candidate,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface(txMessage.FontFamily, txMessage.FontStyle, txMessage.FontWeight, txMessage.FontStretch),
                txMessage.FontSize,
                Brushes.Black,
                new NumberSubstitution(),
                1);

            return new Size(formattedText.Width, formattedText.Height);
        }

        public bool CannotClose { get; set; } = true;
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (CannotClose) e.Cancel = true;
        }
    }
}
