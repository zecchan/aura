﻿using AuraCore.Persona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avatar.DateProvider
{
    public class DateProvider : Aura.IAuraStatePlugin, Aura.IAuraMessageProviderPlugin
    {

        public IEnumerable<string> UpdateTagRequested(Aura.Graphic.Avatar avatar, AuraPersona persona)
        {
            LoadConfig();
            if (Config == null) return null;
            var res = new List<string>();

            if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday || DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                res.Add("weekend");
            else
                res.Add("weekday");
            if (!Config.IsWorkday(DateTime.Today))
                res.Add("dayoff");
            if (Config.IsHoliday(DateTime.Today))
                res.Add("holiday");

            var today = DateTime.Today.DayOfWeek.ToString().ToLower();
            res.Add(today);

            var h = DateTime.Now.Hour;
            var yesterday = DateTime.Today.AddDays(-1).DayOfWeek.ToString().ToLower();
            if (h >= 0 && h < 12)
            {
                res.Add("morning");
                if (h < 6) res.Add("earlymorning");
                res.Add(yesterday + "_night");
            }
            if (h >= 12 && h < 18) res.Add("noon");
            if (h >= 18 && h < 21) res.Add("evening");
            if (h >= 21 && h < 24)
            {
                res.Add("night");
                res.Add(today + "_night");
            }

            var moonAge = MoonAge(DateTime.Today);
            if (moonAge <= 3 || moonAge >= 27) res.Add("newmoon");
            if ((moonAge > 3 && moonAge <= 13) || (moonAge >= 17 && moonAge < 27)) res.Add("halfmoon");
            if (moonAge > 13 && moonAge < 17) res.Add("fullmoon");

            var mmdd = DateTime.Today.Month * 100 + DateTime.Today.Day;
            if (mmdd >= 0320 && mmdd < 0620) res.Add("spring");
            if (mmdd >= 0620 && mmdd < 0922) res.Add("summer");
            if (mmdd >= 0922 && mmdd < 1221) res.Add("fall");
            if (mmdd >= 1221 || mmdd < 0320) res.Add("winter");

            return res;
        }
        private int JulianDate(int d, int m, int y)
        {
            int mm, yy;
            int k1, k2, k3;
            int j;

            yy = y - (int)((12 - m) / 10);
            mm = m + 9;
            if (mm >= 12)
            {
                mm = mm - 12;
            }
            k1 = (int)(365.25 * (yy + 4712));
            k2 = (int)(30.6001 * mm + 0.5);
            k3 = (int)((int)((yy / 100) + 49) * 0.75) - 38;
            // 'j' for dates in Julian calendar:
            j = k1 + k2 + d + 59;
            if (j > 2299160)
            {
                // For Gregorian calendar:
                j = j - k3; // 'j' is the Julian date at 12h UT (Universal Time)
            }
            return j;
        }
        private double MoonAge(int d, int m, int y)
        {
            int j = JulianDate(d, m, y);
            //Calculate the approximate phase of the moon
            var ip = (j + 4.867) / 29.53059;
            ip = ip - Math.Floor(ip);
            double ag;
            if (ip < 0.5)
                ag = ip * 29.53059 + 29.53059 / 2;
            else
                ag = ip * 29.53059 - 29.53059 / 2;
            // Moon's age in days
            ag = Math.Floor(ag) + 1;
            return ag;
        }
        private double MoonAge(DateTime dateTime)
        {
            return MoonAge(dateTime.Day, dateTime.Month, dateTime.Year);
        }

        public IEnumerable<string> FinalizeTagRequested(Aura.Graphic.Avatar avatar, AuraPersona persona)
        {
            return null;
        }

        public DateProviderConfig Config { get; private set; }

        public void LoadConfig()
        {
            Config = Aura.AI.LoadPluginData<DateProviderConfig>("coatl.aura.dateprovider");
        }

        public void SaveConfig()
        {
            if (Config == null) return;
            Aura.AI.SavePluginData("coatl.aura.dateprovider", Config);
        }

        public IEnumerable<string> MessageRequested(Aura.Graphic.Avatar avatar, AuraPersona persona, string eventId, string localeId)
        {
            if (Config == null || Config.Messages == null) return null;
            var re = new List<string>();
            if (eventId == "idle")
            {
                var tomorrow = DateTime.Today.AddDays(1);

                foreach (var msg in Config.Messages)
                {
                    if (msg.Key == "weekend" && (DateTime.Today.DayOfWeek == DayOfWeek.Saturday || DateTime.Today.DayOfWeek == DayOfWeek.Sunday))
                    {
                        re.AddRange(Config.GetMessages(msg.Key));
                    }

                    if (msg.Key == "dayoff" && !Config.IsWorkday(DateTime.Today))
                    {
                        re.AddRange(Config.GetMessages(msg.Key));
                    }
                    if (msg.Key == "dayofftomorrow" && !Config.IsWorkday(tomorrow))
                    {
                        re.AddRange(Config.GetMessages(msg.Key));
                    }

                    if (msg.Key == "holiday" || msg.Key == "holidaytomorrow")
                    {
                        var dt = msg.Key.Contains("tomorrow") ? DateTime.Today.AddDays(1) : DateTime.Today;
                        var evts = Config.GetHoliday(dt);
                        if (evts.Count() > 0)
                            foreach (var r in evts)
                                re.AddRange(Config.GetMessages(msg.Key, r));
                    }

                    if (msg.Key == "anniversary" || msg.Key == "anniversarytomorrow")
                    {
                        var dt = msg.Key.Contains("tomorrow") ? DateTime.Today.AddDays(1) : DateTime.Today;
                        var evts = Config.GetAnniversary(dt);
                        if (evts.Count() > 0)
                            foreach (var r in evts)
                                re.AddRange(Config.GetMessages(msg.Key, r));
                    }

                    if (msg.Key != null && (msg.Key.StartsWith("anniversary:") || msg.Key.StartsWith("anniversarytomorrow:")))
                    {
                        var spl = msg.Key.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        string ctx = null;
                        if (spl.Length == 2)
                            ctx = spl[1].Trim();
                        var dt = msg.Key.Contains("tomorrow") ? DateTime.Today.AddDays(1) : DateTime.Today;
                        var evts = Config.GetAnniversary(dt, ctx);
                        if (evts.Count() > 0)
                        {
                            foreach (var r in evts)
                                re.AddRange(Config.GetMessages(msg.Key, r));
                        }
                    }
                }
            }
            return re;
        }

        public void SaveState()
        {
            SaveConfig();
        }

        public void LoadState()
        {
            LoadConfig();
        }

        public void Shutdown()
        {
        }
    }

    public class DateProviderConfig
    {
        public string[] Workdays { get; set; }
        public DateEvent[] Holidays { get; set; }
        public DateEvent[] Anniversaries { get; set; }
        public Dictionary<string, string[]> Messages { get; } = new Dictionary<string, string[]>();

        public IEnumerable<DateEvent> GetHoliday(DateTime dt)
        {
            return Holidays?.Where(x => x.Date.Date == dt.Date) ?? new List<DateEvent>();
        }
        public IEnumerable<DateEvent> GetAnniversary(DateTime dt, string context = null)
        {
            return Anniversaries?.Where(x => x.Date.Date == dt.Date && x.Context == context) ?? new List<DateEvent>();
        }
        public bool IsWorkday(DateTime dt)
        {
            var dow = dt.DayOfWeek.ToString().ToLower();
            return GetHoliday(dt).Count() == 0 && (Workdays?.Any(x => x.ToLower() == dow) ?? false);
        }

        public bool IsHoliday(DateTime dt)
        {
            return GetHoliday(dt)?.Count() > 0;
        }

        public IEnumerable<string> GetMessages(string key, DateEvent dateEv = null)
        {
            var re = new List<string>();
            if (!Messages.ContainsKey(key))
                return re;
            var msgs = Messages[key];
            if (msgs != null && msgs.Length > 0)
            {
                // basic
                var v0 = dateEv != null ? dateEv.Description : null;
                if (v0 == "@me") v0 = "me";
                if (v0 == "@user") v0 = "you";
                // ownership
                var v1 = dateEv != null ? dateEv.Description : null;
                if (v1 == "@me") v1 = "my";
                else if (v1 == "@user") v1 = "your";
                else v1 += "'s";
                // year
                var yElapsed = dateEv != null ? DateTime.Today.Year - dateEv.Date.Year : 0;
                var v2 = yElapsed.ToString();
                // year-th
                var v3 = yElapsed.ToString() + "th";
                var mdH = yElapsed % 100;
                if (mdH > 20 || (mdH > 0 && mdH < 10))
                {
                    var md = mdH % 10;
                    if (md == 1) v3 = yElapsed + "st";
                    if (md == 2) v3 = yElapsed + "nd";
                    if (md == 3) v3 = yElapsed + "rd";
                }

                foreach (var m in msgs)
                {
                    if (string.IsNullOrWhiteSpace(m)) continue;
                    re.Add(string.Format(m, v0, v1, v2, v3));
                }
            }
            return re;
        }
    }

    public class DateEvent
    {
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Context { get; set; }
    }
}
